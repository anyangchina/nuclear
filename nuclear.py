# coding: utf-8

import re
import datetime
import calendar

import requests
import pandas as pd
import matplotlib.pyplot as plt
import snakemd
from matplotlib.font_manager import FontProperties  # 导入FontProperties

font = FontProperties(fname=r"C:\Windows\Fonts\ChaoZiSheGuoFengRanSongJian-2.ttf", size=15)

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

# 获取时间
now = datetime.date.today()
this_week_start = now - datetime.timedelta(days=now.weekday())
this_week_end = now + datetime.timedelta(days=6-now.weekday())
last_week_start = now - datetime.timedelta(days=now.weekday()+7)
last_week_end = now - datetime.timedelta(days=now.weekday()+1)
this_month_start = datetime.datetime(now.year, now.month, 1)
this_month_end = datetime.datetime(now.year, now.month, calendar.monthrange(now.year, now.month)[1])
last_month_end = this_month_start - datetime.timedelta(days=1)
last_month_start = datetime.datetime(last_month_end.year, last_month_end.month, 1)
month = (now.month - 1) - (now.month - 1) % 3 + 1
this_quarter_start = datetime.datetime(now.year, month, 1)
this_quarter_end = datetime.datetime(now.year, month+2, calendar.monthrange(now.year, month+2)[1])
last_quarter_end = this_quarter_start - datetime.timedelta(days=1)
last_quarter_start = datetime.datetime(last_quarter_end.year, last_quarter_end.month - 2, 1)
this_year_start = datetime.datetime(now.year, 1, 1)
this_year_end = datetime.datetime(now.year + 1, 1, 1) - datetime.timedelta(days=1)
last_year_end = this_year_start - datetime.timedelta(days=1)
last_year_start = datetime.datetime(last_year_end.year, 1, 1)
print("当前时间：", now)
print("当前周：", this_week_start, this_week_end)
print("上周：", last_week_start, last_week_end)
print("当前月：", this_month_start, this_month_end)
print("上月：", last_month_start, last_month_end)
print("当前季度：", this_quarter_start, this_quarter_end)
print("上季：", last_quarter_start, last_quarter_end)
print("当前年：", this_year_start, this_year_end)
print("上年：", last_year_start, last_year_end)

# 数据保存为csv，追加模式
resp = requests.get("https://data.rmtc.org.cn/gis/listtype0M.html")
resp.encoding = "utf-8"
pattern = re.compile(
    '<li class="datali">.*?<div class="divname">.*?<a data-ajax="false".*?>(.*?)</a>.*?</div>.*?<div '
    'class="divval">.*?<span class="label">(.*?)</span>.*?<span class="showtime">(.*?)</span>.*?</li>',
    re.S)
datas = re.findall(pattern, resp.text)
for data in datas:
    _locals, _value, _time = data
    _locals = _locals.strip().split(' ')
    _local1 = _locals[0]
    _local2 = _locals[1].strip('()')
    _value = _value.strip().split(' ')[0]
    _time = _time.strip()
    _one = pd.DataFrame([[_local1, _local2, _value, _time]])
    _one.to_csv('nuclear.csv', mode='a', index=False, header=False)

# 处理csv
df = pd.read_csv('nuclear.csv')
df['date2'] = df['date'].apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d').date())
df = df.sort_values(by=['province', 'date2'], ascending=[True, True])
df = df.drop_duplicates()
df.to_csv('nuclear.csv', index=False)

max_date = df['date2'].max()

"""
plots = [['年度', this_year_start.date(), this_year_end.date()],
         ['季度', this_quarter_start.date(), this_quarter_end.date()],
         ['月度', this_month_start.date(), this_month_end.date()],
         ['周', this_week_start, this_week_end]]
"""
plots = [['年度', last_year_start.date(), last_year_end.date()],
         ['季度', last_quarter_start.date(), last_quarter_end.date()],
         ['月度', last_month_start.date(), last_month_end.date()],
         ['周', last_week_start, last_week_end]]
plot_index = -1
for index, plot in enumerate(plots):
    if max_date == plot[2]:
        plot_index = index
        break

if plot_index >= 0:
    df2 = df[(df['date2'] >= plots[plot_index][1]) & (df['date2'] <= plots[plot_index][2])]

    doc = snakemd.new_doc()
    doc.add_paragraph("---")
    doc.add_paragraph("## 特别说明")
    doc.add_paragraph("吸收剂量是单位质量受照物质所吸收的平均电离辐射能量，单位是J/kg。")
    doc.add_paragraph("专门名词是戈瑞（Gray），符号“Gy”，1Gy = 1J/kg。")
    doc.add_paragraph("这是个很大的单位。")
    doc.add_paragraph("因此在实际应用时，往往用其千分之一或百万分之一作单位，即mGy 、μGy，甚至更小，nGy。")
    doc.add_paragraph("吸收剂量适用于任何类型的辐射和受照物质。")
    doc.add_paragraph("在对环境进行γ辐射监测时，经常用nGy/h作测量单位（吸收剂量率单位），意思是测量地每小时的吸收剂量值。")
    doc.add_paragraph("**正常的天然本底辐射水平视地域的不同而不同，一般在几十到二百nGy/h之间。**")
    doc.add_paragraph("---")
    doc.add_paragraph("时间：**{}~{}**".format(plots[plot_index][1], plots[plot_index][2]))
    _content = []
    for index, dd in df2.iterrows():
        _content.append([dd["province"], dd["value"], dd["date"]])
    doc.add_table(["城市", "辐射值", "时间"], _content)

    provinces = list(set(df['province'].tolist()))
    provinces.sort()
    for province in provinces:
        _d = df[df['province'] == province]
        if not _d.empty:
            _province = _d.iloc[0]['province']
            _city = _d.iloc[0]['city']
            ax = _d.plot(figsize=(16, 9), x='date', y='value', color='blue', label='辐射值(nGy/h)')
            plt.title(_province + ' ' + _city, fontproperties=font)
            plt.grid()
            ax.set_xlabel('')
            plt.savefig('image/all/{}.jpg'.format(_province))
            plt.close()
            doc.add_paragraph("![](https://framagit.org/anyangchina/nuclear/-/raw/main/image/all/{}.jpg?ref_type=heads)"
                              .format(_province))

    doc.add_paragraph("---")
    doc.add_paragraph("## 参考")
    doc.add_paragraph("空气吸收剂量率：https://data.rmtc.org.cn/gis/listtype0M.html")
    doc.add_paragraph("如何理解辐射剂量的量与单位：https://www.cas.cn/zt/sszt/kxjdrbqz/qwjd/201103/t20110322_3091702.html")
    doc.add_paragraph("---")
    doc.add_paragraph("## 最后")
    doc.add_paragraph("本文，")
    doc.add_paragraph("包括本文所有数据图表均由作者Python程序而来，实属不易！~~")
    doc.add_paragraph("请帮忙点赞，分享和在看，谢谢！~")
    doc.add_paragraph("![](https://framagit.org/anyangchina/ppp/-/raw/main/一键三连S.gif?ref_type=heads)")
    doc.dump("全国核辐射检测数据{}表".format(plots[plot_index][0], plots[plot_index][2]))

if __name__ == '__main__':
    pass
